#!/bin/bash

python3 -m rayopt.library $PWD/catalog/zemax/Glasscat
python3 -m rayopt.library $PWD/catalog/zemax/Stockcat
python3 -m rayopt.library $PWD/catalog/oslo/glc
python3 -m rayopt.library $PWD/catalog/oslo/lmo